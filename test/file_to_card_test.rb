require 'simplecov'
SimpleCov.start

require 'minitest/autorun'
require 'file_to_card'

class FileToCardTest < Minitest::Test
  def test_empty_array_clean_captured
    captured = []
    assert_equal([], clean_captured(captured, 0))
  end
end
