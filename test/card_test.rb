require 'simplecov'
SimpleCov.start
require_relative '../lib/crc_card.rb'
require 'minitest/autorun'

class CardTest < Minitest::Test
  def setup
    @card = CrcMaker::Card.new("name")
  end

  def test_name
    assert_equal("name", @card.name)
  end

  def test_initializer_arrays
    assert_equal([], @card.parents)
  end

  def test_add_parent
    @card.add_parent("ParentClass")
    assert_equal(["ParentClass"], @card.parents)
  end

  def test_html_string
    @card.add_parent("ParentClass")
    @card.add_child("ChildClass")
    @card.add_collaboration("collab")
    @card.add_responsibility("kek")
    expected = "<div style=\"padding: 10px; margin-bottom: 5px\"><table style=\"width:100%\"> <tr><td colspan=\"2\"> name <div style=\"float:right\"><ul> Parent classes\t<li> ParentClass </li> </ul><ul>Children<li> ChildClass </li>\t </ul></div></td></tr><tr><td>Class Responsibility<div><ul><li> kek </li></ul></div></td><td>Class Relationship<div><ul><li> collab </li></ul></div></td> </tr></table></div>"
    assert_equal(expected, @card.get_card_html)
  end

end
