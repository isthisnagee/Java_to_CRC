require 'simplecov'
SimpleCov.start

require 'minitest/autorun'
require 'list_to_html_list'

class HtmlTest < Minitest::Test

  def test_html_list
    test_this = ["abc","123","xyz"]
    expected = "<li> abc </li><li> 123 </li><li> xyz </li>"
    assert_equal(expected, test_this.array_to_html_list_string)
  end

  def test_empty
    assert_equal("", [].array_to_html_list_string)
  end

  def test_integers
    test_this = [1,2,3]
    expected = "<li> 1 </li><li> 2 </li><li> 3 </li>"
    assert_equal(expected, test_this.array_to_html_list_string)
  end	
end

