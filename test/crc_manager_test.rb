require 'simplecov'
SimpleCov.start

require 'minitest/autorun'
require 'crc_manager'
require 'crc_card'


class ManagerTest < Minitest::Test
  def setup
    @manager = CrcMaker::CardManager.new
    @card = CrcMaker::Card.new("Word")
  end

  def test_initialization
    assert_equal @manager.cards, []
  end

  def test_correct_addition
    @manager.add_card(@card)
    assert_equal @manager.cards.length, 1
  end

  def test_raises_type_excpetion
    assert_raises TypeError do
      @manager.add_card(1)
    end
  end

  def test_remove_existing_card
    @manager.add_card(@card)
    @manager.remove_card("Word")
    assert_equal @manager.cards, []
  end

  def test_remove_from_empty
    @manager.remove_card("blank")
    assert_equal @manager.cards, []
  end

  def test_remove_non_existing_card
    @manager.add_card(@card)
    @manager.remove_card("hello")
    assert_equal @manager.cards, [@card]
  end

  def test_html_page
    @card.add_child("name")
    @card.add_parent("Name")
    @card.add_responsibility("works")
    @card.add_collaboration("collabzz")
    @manager.add_card(@card)
    expected = "<!DOCTYPE HTML><head></head><body><style>table { padding: 0; } table tr { border-top: 1px solid #cccccc; background-color: white; margin: 0; padding: 0; } table tr:nth-child(2n) { background-color: #f8f8f8; } table tr th { font-weight: bold; border: 1px solid #cccccc; text-align: left; margin: 0; padding: 6px 13px; } table tr td { border: 1px solid #cccccc; text-align: left; margin: 0; padding: 6px 13px; } table tr th :first-child, table tr td :first-child { margin-top: 0; } table tr th :last-child, table tr td :last-child { margin-bottom: 0; }</style><div style=\"padding: 10px; margin-bottom: 5px\"><table style=\"width:100%\"> <tr><td colspan=\"2\"> Word <div style=\"float:right\"><ul> Parent classes\t<li> Name </li> </ul><ul>Children<li> name </li>\t </ul></div></td></tr><tr><td>Class Responsibility<div><ul><li> works </li></ul></div></td><td>Class Relationship<div><ul><li> collabzz </li></ul></div></td> </tr></table></div></body></html>"
    assert_equal(expected, @manager.get_html_page)
  end
end

