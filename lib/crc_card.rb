require_relative "crc_maker/version"
require_relative "list_to_html_list"


# @author isthisnagee
module CrcMaker
  class Card

    attr_reader :name, :parents, :children, :responsibilities, :collaboration

    # Initialize a CRC card 
    # @param name [String] the class name for the CRC card
    def initialize(name)
      @name             = name
      @parents          = []
      @children         = []
      @responsibilities = []
      @collaboration    = []
    end

    # Add a parent class to this CRC card. Note that java lets a class
    # extend only one class, so this card can only have one parent_class.
    # If a new parent is added, the older is removed.
    # 
    # @param parent_class [String] the name of the parent class
    def add_parent(parent_class)
      parents.push(parent_class)
    end

    # Add a child class to this class
    #
    # @param child_class [String] the name of a child class.  
    def add_child(child_class)
      children.push(child_class)
    end

    # Add a responsibility (essentially a method) to this class.
    #
    # @param r [String] the name of a responsibility.
    def add_responsibility(r)
      responsibilities.push(r)
    end

    # Add collaboration c to this card. Collaboration is any class that is imported.
    #
    # @param c [String] the name of a class this class collaborates with.
    def add_collaboration(c)
      collaboration.push(c)
    end

    # Converts the data in this Card to an html table.
    #
    # @return [String] the html representation of this card.
    def get_card_html
      string = "<div style=\"padding: 10px; margin-bottom: 5px\"><table style=\"width:100%\"> <tr><td colspan=\"2\"> #{name} <div style=\"float:right\"><ul> Parent classes  #{parents.array_to_html_list_string} </ul><ul>Children#{children.array_to_html_list_string}   </ul></div></td></tr><tr><td>Class Responsibility<div><ul>#{responsibilities.array_to_html_list_string}</ul></div></td><td>Class Relationship<div><ul>#{collaboration.array_to_html_list_string}</ul></div></td> </tr></table></div>"

      return string
    end

  end

end
