require_relative "crc_maker/version"
require_relative 'crc_card'

def clean_captured(captured, delim)
  cc = []
  i = 0
  for c in captured
    if not c.nil? || c.empty?
      cc.push(c[delim].strip)
      i += 1
    end
  end
  return cc
end


def get_imports(content)
  captured = content.scan(/import ((?!android|java|junit))(.+);/)
  imports = clean_captured(captured, 1)
  clean_imports = []
  for i in imports
    ci = i.split(".")[1]
    clean_imports.push(ci)
  end
  return clean_imports
end

def get_functions(content)
  captured = content.scan(/(public|protected|private|static|\s) +[\w\<\>\[\]]+\s+(\w+) *\([^\)]*\) *(\{?|[^;])/)
  return    clean_captured(captured, 1)
end

def get_interfaces(content)
  captured = content.scan(/(class|abstract) .+ implements (.+)\s*(.*) {/)
  interfaces = clean_captured(captured, 1)
  separated_interfaces = []
  interfaces.each{|i| i.split(",").each{|x| separated_interfaces.push(x.strip) } }
  return separated_interfaces 
end

def get_parent(content)
  captured = content.scan(/(class|abstract) .+ extends (\w+)/)
  return    clean_captured(captured, 1)
end



def file_to_card(path)
  file = File.open(path)
  content = ""
  file.each {|line| content << line }
  title = content[/(class|abstract|interface)( class| abstract| interface)? (\w+)/, 3]
  imports = get_imports(content)
  functions = get_functions(content)
  interfaces = get_interfaces(content)
  parents = get_parent(content)

  card = CrcMaker::Card.new(title)
  imports.each{|i| card.add_collaboration(i)}
  functions.each{|f| card.add_responsibility(f)}
  interfaces.each{|i| card.add_parent(i)}
  parents.each{|p| card.add_parent(p)}
  return card    
end


