require_relative "crc_maker/version"
require_relative "list_to_html_list"

module CrcMaker
  class CardManager

    attr_reader :cards

    def initialize
      @cards = []
    end

    def add_card(card)
      begin
        if card.instance_of? Card
          cards.push(card)
          return true
        end
        raise TypeError
      rescue
        raise TypeError
      end
      return false
    end

    def remove_card(name)
      cards.delete_if { |card| card.name == name}
    end

    def get_html_page
      s = "<!DOCTYPE HTML><head></head><body>"
      style = "<style>table { padding: 0; } table tr { border-top: 1px solid #cccccc; background-color: white; margin: 0; padding: 0; } table tr:nth-child(2n) { background-color: #f8f8f8; } table tr th { font-weight: bold; border: 1px solid #cccccc; text-align: left; margin: 0; padding: 6px 13px; } table tr td { border: 1px solid #cccccc; text-align: left; margin: 0; padding: 6px 13px; } table tr th :first-child, table tr td :first-child { margin-top: 0; } table tr th :last-child, table tr td :last-child { margin-bottom: 0; }</style>"

      s += style
      for card in cards
        s += card.get_card_html
      end
      s += "</body></html>"
      return s
    end

  end
end
