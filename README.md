# CrcMaker

CrcMaker is for CSC207 students at the University of Toronto. Part of the requirement for the "big project" is to 
have an updated CRC card pdf file at the end of every development phase (phase 1, phase 2, phase 3a, phase 3b).

Obviously, at the beginning of each phase, the students should update/make CRC cards by hand, and this tool won't be useful
because the new things to be added will not be implemented yet. Use [this crc maker](http://echeung.me/crcmaker/) for that.

However, at the end of a phase, if the student's don't
update thier cards whenever they make changes to the design, than this tool should be used. It just makes life easier.



## Prerequisites

Have [Ruby](https://www.ruby-lang.org/en/documentation/installation/) and [bundler](http://bundler.io) installed.


## Installation

Download this repo.

Run 

```$ bundle install```

## Usage

To get the crc cards for your Java files, run 

```$ bin/makecrc path/to/head/of/java/project```

For example:

```/Users/isthisnagee/pIII/FlightPicker``` is the top of my android project, and running

```$ bin/makecrc /Users/isthisnagee/pIII/FlightPicker```

Will place a pdf in the directory `FlightPicker`



## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/isthisnagee/java_to_crc. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

